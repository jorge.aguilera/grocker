package com.puravida.docker

/**
 * Created by jorge on 31/01/17.
 */
import asteroid.Local
import com.puravida.docker.ast.DockerTransform

@Local(value=DockerTransform)
@interface Grocker {
    String image()
}
