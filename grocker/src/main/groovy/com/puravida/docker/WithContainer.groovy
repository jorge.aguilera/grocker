package com.puravida.docker
/**
 * Created by jorge on 13/02/17.
 */
import asteroid.Local
import com.puravida.docker.ast.WithContainerTransform

@Local(
        value=WithContainerTransform,
        applyTo = Local.TO.METHOD)
@interface WithContainer {
    String name()
    String cmd()
    String env()
    String ports()
}
