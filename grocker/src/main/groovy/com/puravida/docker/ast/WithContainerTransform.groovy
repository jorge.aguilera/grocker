package com.puravida.docker.ast

/**
 * Created by jorge on 13/02/17.
 */
import asteroid.A
import asteroid.Phase
import asteroid.Phase.LOCAL
import asteroid.AbstractLocalTransformation
import asteroid.Utils
import com.puravida.docker.WithContainer
import groovy.transform.CompileStatic

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.stmt.*

@CompileStatic
@Phase(LOCAL.SEMANTIC_ANALYSIS)
class WithContainerTransform extends AbstractLocalTransformation<WithContainer, MethodNode>{

    @Override
    void doVisit(final AnnotationNode annotation, final MethodNode methodNode) {

        String name = Utils.ANNOTATION.get(annotation,'name',String)
        if( !name){
            addError("Name for container is required", annotation)
            return
        }
        String cmd = Utils.ANNOTATION.get(annotation,'cmd',String)
        String env = Utils.ANNOTATION.get(annotation,'env',String)
        String ports = Utils.ANNOTATION.get(annotation,'ports',String)

        def oldCode   = methodNode.code
        def startCode = startContainer([name:name,cmd:cmd,env:env,ports:ports])
        def endCode   = stopContainer()

        methodNode.code = A.STMT.blockS(startCode, oldCode, endCode)
    }

    Statement startContainer( Map<String,String> values) {
        return A.STMT.blockS(
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'setName', A.EXPR.constX(values.name) )),
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'setCmd', A.EXPR.constX(values.cmd) )),
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'setEnv', A.EXPR.constX(values.env) )),
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'setPorts', A.EXPR.constX(values.ports) )),
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'startContainer' )),
        )
    }

    Statement stopContainer( ) {
        return A.STMT.blockS(
                A.STMT.stmt(A.EXPR.callX( A.EXPR.varX('dockerMgr'), 'stopContainer' ))
        )
    }
}
