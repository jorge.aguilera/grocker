package com.puravida.docker.ast

/**
 * Created by jorge on 4/02/17.
 */
import static asteroid.Phase.GLOBAL

import groovy.transform.CompileStatic

import asteroid.Phase
import asteroid.AbstractGlobalTransformation
import asteroid.transformer.Transformer

@CompileStatic
@Phase(GLOBAL.CONVERSION)
class AddTransformation extends AbstractGlobalTransformation {

    @Override
    List<Class<Transformer>> getTransformers() {
        return [AddImportTransformer]
    }
}
