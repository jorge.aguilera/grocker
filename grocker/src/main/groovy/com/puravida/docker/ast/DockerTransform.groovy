package com.puravida.docker.ast

/**
 * Created by jorge on 31/01/17.
 */
import asteroid.A
import asteroid.Phase
import asteroid.Phase.LOCAL
import asteroid.AbstractLocalTransformation
import asteroid.Utils
import com.puravida.docker.Grocker
import com.puravida.docker.mgr.DockerMgr
import groovy.transform.CompileStatic

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*

@CompileStatic
@Phase(LOCAL.SEMANTIC_ANALYSIS)
class DockerTransform extends AbstractLocalTransformation<Grocker, ClassNode>{

    @Override
    void doVisit(AnnotationNode annotation, ClassNode node) {
        println "hooaoaoa"
        try {
            String image = Utils.ANNOTATION.get(annotation, 'image', String)
            if( !image ){
                addError("Image attribute is required", annotation)
                return
            }

            A.UTIL.CLASS.addProperty(node, A.NODES
                    .property('dockerMgr')
                    .modifiers(A.ACC.ACC_PROTECTED)
                    .type(DockerMgr)
                    .initialValueExpression(initDockerMgrExpr(image))
                    .build()
            )

        }catch(Throwable e){
            println e.message
        }
    }

    private Expression initDockerMgrExpr(final String image){
        A.EXPR.staticCallX(DockerMgr,'newInstance',A.EXPR.varX('this'),A.EXPR.constX(image))
    }

}
