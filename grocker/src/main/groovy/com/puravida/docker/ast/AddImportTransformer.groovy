package com.puravida.docker.ast

import asteroid.A
import asteroid.transformer.AbstractClassNodeTransformer
import com.puravida.docker.Grocker
import com.puravida.docker.mgr.DockerMgr
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.control.SourceUnit

/**
 * Created by jorge on 4/02/17.
 */
class AddImportTransformer extends AbstractClassNodeTransformer {

    public AddImportTransformer(final SourceUnit sourceUnit) {
        super(sourceUnit, byAnnotationName(Grocker.simpleName))
    }

    /**
     * {@inheritDocs}
     */
    @Override
    void transformClass(final ClassNode target) {
        A.UTIL.CLASS.addImport(target, DockerMgr)
    }
}