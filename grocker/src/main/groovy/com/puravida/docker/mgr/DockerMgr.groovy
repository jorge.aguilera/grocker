package com.puravida.docker.mgr

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.command.CreateContainerCmd
import com.github.dockerjava.api.command.CreateContainerResponse
import com.github.dockerjava.api.command.DockerCmdExecFactory
import com.github.dockerjava.api.model.Event
import com.github.dockerjava.api.model.ExposedPort
import com.github.dockerjava.api.model.Ports
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientBuilder
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.core.command.EventsResultCallback
import groovy.util.logging.Log

/**
 * Created by jorge on 15/02/17.
 */
@Log
class DockerMgr {

    DockerClientConfig dockerClientConfig
    DockerCmdExecFactory dockerCmdExecFactory
    DockerClient dockerClient

    Object parent
    String image

    static DockerMgr newInstance( parent, image){
        DockerMgr ret = new DockerMgr(parent: parent, image: image).init()
        return ret
    }

    private DockerMgr init(){
        assert parent, "Parent is required"
        assert image, "Image is required"

        dockerClientConfig = parent.hasProperty('dockerClientConfig') && parent.dockerClientConfig ?
                parent.dockerClientConfig :
                DefaultDockerClientConfig.createDefaultConfigBuilder().build()

        dockerCmdExecFactory = parent.hasProperty('dockerCmdExecFactory') && parent.dockerCmdExecFactory ?
                parent.dockerCmdExecFactory :
                DockerClientBuilder.getDefaultDockerCmdExecFactory()

        dockerClient = DockerClientBuilder.getInstance(dockerClientConfig)
                .withDockerCmdExecFactory(dockerCmdExecFactory)
                .build();

        log.info "${dockerClient.infoCmd().exec()}"
        log.info "${dockerClient.searchImagesCmd(image).exec()}"
        this
    }

    String name
    String cmd
    String env
    String ports

    CreateContainerResponse container
    void startContainer( ){
        log.info "startContainer $image $name $cmd $env"

        CreateContainerCmd containerCmd = dockerClient.createContainerCmd(image).withName(name)
        if( cmd && cmd.length() ){
            containerCmd = containerCmd.withCmd([cmd])
        }
        if( env && env.length()){
            containerCmd = containerCmd.withEnv(env.split(';'))
        }
        if( ports && ports.length()){
            ExposedPort tcp = ExposedPort.tcp(ports as int);
            Ports portBindings = new Ports();
            portBindings.bind(tcp, Ports.Binding.bindPort(ports as int))
            containerCmd = containerCmd.withExposedPorts(tcp).withPortBindings(portBindings)
        }
        container = containerCmd.exec()
        dockerClient.startContainerCmd(container.id).exec();
    }

    void stopContainer(){
        log.info "stopContainer $image $name"
        dockerClient.stopContainerCmd(container.id).exec();
        dockerClient.removeContainerCmd(container.id).exec()
    }
}
