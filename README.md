# Grocker  (groovy & docker)

Mi primera anotación para Groovy.

Sigue el proyecto en su blog http://jorge.aguilera.gitlab.io/grocker/

Anota una clase con @Grocker indicando la imagen docker a usar y anota
los métodos de interés con @WithContainer para que el container se inicie
al comienzo de tu código y se pare al salir.

## Test de integracion

```groovy
@RunWith(JUnit4.class)
@Grocker(image='mysql/mysql-server:5.5')
class SimpleTest extends GroovyTestCase{

    @Test
    @WithContainer(name ='mysql_grocker_ok',
            cmd = '',
            env = "MYSQL_ROOT_PASSWORD=my-secret-pwd;MYSQL_DATABASE=test;MYSQL_USER=user1;MYSQL_PASSWORD=password1",
            ports = "3306")
    void testSimple(){

        Sql sql = Sql.newInstance("jdbc:mysql://127.0.0.1/test","user1","password1")

        sql.execute '''
             create table PROJECT (
                 id integer not null,
                 name varchar(50),
                 url varchar(100)
             )
        '''

        sql.eachRow "select * from PROJECT",{
            println it
        }
    }

    @Test
    @WithContainer(name ='mysql_grocker_error',
            cmd = '',
            env = "MYSQL_ROOT_PASSWORD=my-secret-pwd;MYSQL_DATABASE=test;MYSQL_USER=user1;MYSQL_PASSWORD=password1",
            ports = "3306")
    void testOther(){
        
        Sql sql = Sql.newInstance("jdbc:mysql://127.0.0.1/test","user1","password1")

        sql.execute '''
             create table EMPLOYEE (
                 id integer not null,
                 name varchar(50)
             )
        '''

        sql.eachRow "select * from EMPLOYEE",{
            println it
        }
    }

}

```


