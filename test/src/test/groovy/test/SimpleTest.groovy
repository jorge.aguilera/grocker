package test

import com.puravida.docker.Grocker
import com.puravida.docker.WithContainer
import groovy.sql.Sql
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by jorge on 31/01/17.
 */
@RunWith(JUnit4.class)
@Grocker(image='mysql/mysql-server:5.5')
class SimpleTest extends GroovyTestCase{

    @Test
    @WithContainer(name ='mysql_grocker_ok',
            cmd = '',
            env = "MYSQL_ROOT_PASSWORD=my-secret-pwd;MYSQL_DATABASE=test;MYSQL_USER=user1;MYSQL_PASSWORD=password1",
            ports = "3306")
    void testSimple(){

        sleep(10000)

        Sql sql = Sql.newInstance("jdbc:mysql://127.0.0.1/test","user1","password1")

        sql.execute '''
             create table PROJECT (
                 id integer not null,
                 name varchar(50),
                 url varchar(100)
             )
        '''

        sql.eachRow "select * from PROJECT",{
            println it
        }
    }

}
